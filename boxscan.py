import os, sys
import nmap3
import json
import argparse
import pyfiglet
import time
from colorama import Fore, Style

start_time = time.time()

# Affichage banniere
ascii_banner = pyfiglet.figlet_format("BoxScan")
print(ascii_banner + "By Libereau")
print("\n --------------------------------------- \n")
print("\nCurrent working directory : "+str(os.getcwd())+"\n")

# Définition des arguments obligatoires et facultatifs
parser = argparse.ArgumentParser()
parser.add_argument("-i", "--ip", required=True, help="IP to scan")
parser.add_argument("-w", "--wordlists", required=False, type=str, help="Default wordlist : /usr/share/dirbuster/wordlists/directory-list-2.3-medium.txt")

# Récupération des arguments passés au lancement du script
args = parser.parse_args()
tab_port = []
ip = args.ip
wordlist = args.wordlists

# Instantiation de la lib nmap en tant que variable globale
nmap = nmap3.NmapScanTechniques()

# ===================================================================================
# ============================== Fonction de ping scan ==============================
# ===================================================================================

def pingScan():
    print("TCP scan of "+ip+"\n")

    # Création du réperttoire scan_<IP>, s'il existe, affichage message d'erreur
    try:
        os.system("mkdir -m 777 scan_"+ip)
    except:
        pass

    # Ouverture du fichier de ping scan en écriture
    file_ping = open("scan_"+ip+"/ping_"+ip, "w")

    # Ping scan
    results = nmap.nmap_ping_scan(ip)

    # Ecriture du résultat du ping scan dans le fichier file_ping
    json.dump(results, file_ping)

    # Fermture du fichier
    file_ping.close()

    # Ouverture du fichier de ping scan en lecture
    file_ping = open("scan_"+ip+"/ping_"+ip, "r")

    # Récupération des données en format json
    data = json.load(file_ping)

    # Si la variable state est présente dans le retour, la machine est up
    try:
        state = data[ip]["state"]["state"]
        # Affichage du statut up de la machine
        print(Fore.GREEN + f"State : {state}" + Style.RESET_ALL)
        scan()

    # Si machine down, on demande à l'utilisateur s'il veut continuer ou non (possible machine Win qui ne répond pas au scan)
    except:
        sys.exit(Fore.RED + "Host is down." + Style.RESET_ALL)
        saisie = input("\nContinue ? (enter pour continuer, sinon exit)")

        if len(saisie) != 0 :
            exit("\nA plus !\n")

        print("\nLancement du scan avec la machine ne répondant pas au ping..\n")
        scan()



# ===================================================================================
# ================================ Fonction de scan =================================
# ===================================================================================


def scan():
    print(Fore.RED + "\n-- Scan --\n" + Style.RESET_ALL)

    # Ouverture du fichier de scan en écriture
    defaultScan = open("scan_"+ip+"/Scan_"+ip, "w")

    # Commande de scan de l'ip, sur les 100 ports les plus communs
    results = nmap.scan_top_ports(ip, 100, args="-A -Pn")

    # Ecriture du résultat dans le fichier de scan
    json.dump(results, defaultScan)

    # Ouverture du fichier de scan en lecture
    defaultScan = open("scan_"+ip+"/Scan_"+ip, "r")

    #Récupération des infos sous la forme d'un json
    data = json.load(defaultScan)

    # Définition des variables
    name = ""
    version = ""
    product = ""
    output = ""

    # récupération des ports ouverts
    tab_ports = data[ip]["ports"]

    # On parcourt le tableau de ports
    for i in tab_ports:

        port = i['portid']
        tab_port.append(port)
        output += "[+] "+port+" "

        # Récupération du nom du service
        if "name" in i['service']:
            name = i['service']['name']
            output += "\t"+name

            # Récupération du nom du produit
            if 'product' in i['service']:
                product = i['service']['product']
                output += "\t"+product

                # Récupération de la version du service
                if 'version' in i['service']:
                    version = i['service']['version']
                    output += "\t"+version

        print(Fore.GREEN + output + Style.RESET_ALL)
        output = ""

    # Lancement dans un sous shell de la commande de scan complet
    os.system("python3 ~/Documents/programmes/BoxScan/full_scan_server.py -i "+ip+" &")


# ===========================================================================================================
# ================================ Fonction de scan des services identifiés =================================
# ===========================================================================================================


def openPort(ip):
    for port in tab_port:
        # Scan sur le port 21 "ftp" à l'aide de script nmap
        if port == "21":
            print(Fore.RED + "\n[+] FTP Server \n" + Style.RESET_ALL)
            cmd = f"sudo nmap --script=ftp-anon.nse -p 21 {ip}"
            os.system(cmd)

        # Scan sur le port 53 "dns" à l'aide de dnsenum, dnsrecon et transfert de zone
        elif port == "53":
            domain_name = input("\nEnter domain name (name or nothing): ")

            if domain_name != "":
                scan1 = "dnsenum "+domain_name
                os.system(scan1)

                scan2 = "dnsrecon "+domain_name
                os.system(scan2)

                # transfer zone
                scan3 = "dnsrecon -d "+domain_name+" -a"
                os.system(scan3)

                scan4 = "host -t axfr "+domain_name+" "+ip
                os.system(scan4)

            else :
                print("Skiping..\n")


        elif port == "161":
            print(Fore.RED + "\n[+] SNMP\n" + Style.RESET_ALL)
            # TODO

        # Scan sur les ports 445 - 139 "SMB" à l'aide d'enum4linux en anonyme, et smbclient
        elif port == "445" or port == 139 :
            print(Fore.RED + "\n[+] Smb enumeration\n" + Style.RESET_ALL)
            print(Fore.RED + "[-] Enum4Linux\n" + Style.RESET_ALL)
            cmd1 = "enum4linux -a "+ip
            os.system(cmd1)
            print(Fore.RED + "[-] Smbclient" + Style.RESET_ALL)
            cmd2 = "smbclient -L "+ip
            os.system(cmd2)

        # Scan sur le port 2049 - "NFS"
        elif port == "2049":
            print(Fore.RED + "\n[+] NFS\n" + Style.RESET_ALL)
            cmd1 = "showmount -e "+ip
            os.system(cmd1)

            print("\nCommand to perform next : ")
            print("\tsudo mount -v -t nfs "+ip+":<SHARE> <DIRECTORY>")

        # Scan des services VNC
        elif port == "5800" or port == "58001" or port == "5900" or port == "5901":
            print(Fore.RED + "\n[+] VNC enumeration\n" + Style.RESET_ALL)
            cmd1 = "nmap -sV --script=vnc-info,realvnc-auth-bypass,vnc-title -v -p "+port+" "+ip
            os.system(cmd1)

            cmd2 = "vncviewer "+ip+" "+port
            os.system(cmd2)

    # Sortie de boucle pour tester uniquement les ports web 80 et 443
    if ("80" in tab_port) or ("443" in tab_port) :
        print(Fore.RED + "\n[+] Web Server\n" + Style.RESET_ALL)

        defa_wordlist = "/usr/share/dirbuster/wordlists/directory-list-2.3-medium.txt"

        if wordlist != None:
            defa_wordlist = wordlist

        print(Fore.GREEN + "\t[+] Using this wordlist : "+defa_wordlist + Style.RESET_ALL)

        choices = ["gobuster","directory", ""]
        print(Fore.RED + "\n[!] Actions : \n" + Style.RESET_ALL)
        print(Fore.GREEN + "\t[-] gobuster" + Style.RESET_ALL)
        print(Fore.GREEN + "\t[-] directory : directory for enumeration" + Style.RESET_ALL)
        print(Fore.GREEN + "\t[-] Stop, press Enter" + Style.RESET_ALL)
        choice = input("Choice : ")

        while choice not in choices:
            print(Fore.RED + "\n[!] Actions : \n" + Style.RESET_ALL)
            print(Fore.GREEN + "\t[-] gobuster" + Style.RESET_ALL)
            print(Fore.GREEN + "\t[-] directory : directory for enumeration" + Style.RESET_ALL)
            print(Fore.GREEN + "\t[-] Stop, press Enter" + Style.RESET_ALL)
            choice = input("Choice : ")

        if choice == "gobuster":
            print("Running gobuster.. \n\n")
            cmd = "gobuster dir -w "+defa_wordlist+" -x txt,html,sh,php -u http://"+ip+" -r"
            os.system(cmd)

        elif choice == "directory":
            dir = input("Directory to add : ")
            ip += "/"+dir # 127.0.0.1/bonjour
            print("Running gobuster.. \n")
            cmd = "gobuster dir -w "+defa_wordlist+" -x txt,html,sh,php -u http://"+ip
            os.system(cmd)

        else :
            pass

pingScan()
openPort(ip)

def end():
    print("\n[+] Think to run UDP scan !")
    print("\t[!] Try that : nmap -A -sU -p- "+ip)

stop_time = time.time()
execution_time = round(stop_time - start_time, 1)
print("\n[+] Executed in "+str(execution_time)+"secs")
