import os
import argparse

parser = argparse.ArgumentParser()

parser.add_argument("-i", "--ip", required=True, help="IP to scan")
args = parser.parse_args()
ip = args.ip

def scan_open():
    user = os.uname()[1]
    nmap_parse = "/home/"+user+"/nmap-parse-output/nmap-parse-output"

    if os.path.exists(nmap_parse):
        os.system("nmap "+ip+" --min-rate 4000 -p- -A -sC -sV -T5 --version-intensity 8 -oX scan_"+ip+"/output.xml >/dev/null")
        os.system(nmap_parse+" scan_"+ip+"/output.xml html > scan_"+ip+"/output.html")
        os.system("firefox scan_"+ip+"/output.html")

    else :
        exit("[+] Refer to requirement.txt, nmap-parse is missing !")

scan_open()
